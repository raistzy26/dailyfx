//
//  APIConfiguration.swift
//  DailyFX
//
//  Created by Tine Ramos on 10/01/2022.
//

import Foundation

protocol APIConfiguration {

    var host: URL { get }
    var version: String { get }
    
}

extension APIConfiguration {
    
    var baseURL: URL {
        URL(string: "\(host)/\(version)/")!
    }
    
}
