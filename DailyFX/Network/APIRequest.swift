//
//  APIRequest.swift
//  DailyFX
//
//  Created by Tine Ramos on 09/01/2022.
//

import Foundation

struct APIRequest {
    
    enum HTTPMethod: String {
        case get = "GET"
        case post = "POST"
    }
    
    let apiConfiguration: APIConfiguration
    let path: String
    
    let httpMethod: HTTPMethod
    let headers: [String: String]

    var urlRequest: URLRequest {

        let url = URL(string: path, relativeTo: apiConfiguration.baseURL)!
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = httpMethod.rawValue
        
        for header in headers {
            urlRequest.setValue(header.value, forHTTPHeaderField: header.key)
        }

        return urlRequest
    }
}
