//
//  NetworkManager.swift
//  DailyFX
//
//  Created by Tine Ramos on 09/01/2022.
//

import Foundation

protocol NetworkManagerProtocol {
    
    var apiConfiguration: APIConfiguration { get }

}
