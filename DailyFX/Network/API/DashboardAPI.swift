//
//  DashboardAPI.swift
//  DailyFX
//
//  Created by Tine Ramos on 09/01/2022.
//

import Foundation

// Note: This object is supposed to be a 1-2-1 mapping of the JSON response however, it's difficult to assume their types and optionality without the right documentation so I've only include a few of them.

// Maybe these structures can just conform to `Decodable` as there's no need to convert them back to a JSON once decoded

enum DashboardAPI {

    struct Request {}
    
    struct Response {
        
        struct Dashboard: Decodable {
            
            struct News: Decodable {
                
                struct Author: Decodable {
                    let name: String
                    let title: String?
                    let bio: String?
                    let descriptionShort: String?
                    let photo: String?
                }

                let title: String
                let url: String
                let description: String
                let headlineImageUrl: String
                let newsKeywords: String?
                let authors: [Author]
                let instruments: [String]?
                let tags: [String]
                let categories: [String]
                let displayTimestamp: Double
                let lastUpdatedTimestamp: Double
            }

            let breakingNews: [News]?
            let topNews: [News]
            let dailyBriefings: [String: [News]]
            let technicalAnalysis: [News]
            let specialReport: [News]
        }
        
    }
    
}

extension DashboardAPI.Request {
    
    static func urlRequest() -> APIRequest {
        
        APIRequest(apiConfiguration: DailyFXAPIConfiguration(),
                   path: "dashboard",
                   httpMethod: .get,
                   headers: ["Content-Type": "application/json"])
        
    }
    
}
