//
//  MarketsAPI.swift
//  DailyFX
//
//  Created by Tine Ramos on 09/01/2022.
//

import Foundation

enum MarketAPI {
    
    struct Request {}
    
    struct Response {
        
        struct Market: Decodable {
            
            struct Item: Decodable {
                
                let displayName: String
                let marketId: String
                let epic: String
                let rateDetailURL: String
                let topMarket: Bool
                let unscalingFactor: Int
                let unscaledDecimals: Int
                let caledarMapping: [String]
                
            }
            
            let currencies: [Item]
            let commodities: [Item]
            let indices: [Item]
            
        }
        
    }
    
}

extension MarketAPI.Request {
    
    static func urlRequest() -> APIRequest {
        
        APIRequest(apiConfiguration: DailyFXAPIConfiguration(),
                   path: "markets",
                   httpMethod: .get,
                   headers: ["Content-Type": "application/json"])
        
    }
    
}
