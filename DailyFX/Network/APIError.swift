//
//  APIError.swift
//  DailyFX
//
//  Created by Tine Ramos on 10/01/2022.
//

import Foundation

enum APIError: Error {
    case invalidResponse
    case generic
}
