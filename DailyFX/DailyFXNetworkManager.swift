//
//  DailyFXNetworkManager.swift
//  DailyFX
//
//  Created by Tine Ramos on 09/01/2022.
//

import Foundation

struct DailyFXAPIConfiguration: APIConfiguration {
    
    let host: URL = URL(string: "https://content.dailyfx.com/api/")!
    let version: String = "v1"

}

protocol DailyFXAPI: NetworkManagerProtocol {
    
    func getDashboard(completion: @escaping (Result<DashboardAPI.Response.Dashboard, APIError>) -> Void)
    func getMarkets(completion: @escaping (Result<MarketAPI.Response.Market, APIError>) -> Void)
    
}

class DailyFXNetworkManager: DailyFXAPI {

    let apiConfiguration: APIConfiguration
    
    init(apiConfiguration: APIConfiguration = DailyFXAPIConfiguration()) {
        self.apiConfiguration = apiConfiguration
    }
    
    func getDashboard(completion: @escaping (Result<DashboardAPI.Response.Dashboard, APIError>) -> Void) {

        let dashboardAPIRequest = DashboardAPI.Request.urlRequest()
        
        executeRequest(dashboardAPIRequest) { result in
            switch result {
            case .success(let data):
                
                if let dashboard = try? JSONDecoder().decode(DashboardAPI.Response.Dashboard.self, from: data) {
                    completion(.success(dashboard))
                } else {
                    completion(.failure(.invalidResponse))
                }
                
            case .failure:
                completion(.failure(.generic))
            }
        }

    }
    
    func getMarkets(completion: @escaping (Result<MarketAPI.Response.Market, APIError>) -> Void) {
        
        let marketAPIRequest = MarketAPI.Request.urlRequest()
        
        executeRequest(marketAPIRequest) { result in
            switch result {
            case .success(let data):
                
                if let market = try? JSONDecoder().decode(MarketAPI.Response.Market.self, from: data) {
                    completion(.success(market))
                } else {
                    completion(.failure(.invalidResponse))
                }
                
            case .failure:
                completion(.failure(.generic))
            }
        }
        
    }
    
    private func executeRequest(_ apiRequest: APIRequest, completion: @escaping (Result<Data, Error>) -> Void) {
        
        let task = URLSession.shared.dataTask(with: apiRequest.urlRequest) { data, urlResponse, error in
            
            if let data = data {
                completion(.success(data))
            } else if let error = error {
                completion(.failure(error))
            }
            
        }

        task.resume()

    }
    
}
