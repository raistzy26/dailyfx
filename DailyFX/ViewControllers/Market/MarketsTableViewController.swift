//
//  MarketsTableViewController.swift
//  DailyFX
//
//  Created by Tine Ramos on 09/01/2022.
//

import Foundation
import UIKit

final class MarketsTableViewController: UIViewController, UITableViewDataSource {
    
    lazy var tableView: UITableView = {
        return UITableView()
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
    }
    
    // MARK: - DataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        return UITableViewCell()
        
    }
    
}
