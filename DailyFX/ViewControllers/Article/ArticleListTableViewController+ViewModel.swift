//
//  ArticleListTableViewController+ViewModel.swift
//  DailyFX
//
//  Created by Tine Ramos on 10/01/2022.
//

import Foundation

extension ArticleListTableViewController {
    
    struct ViewModel {
        
        enum State {
            case loading
            case loaded([NewsType])
        }
        
        enum Continent {
            case asia, eu, us
        }
        
        enum NewsType {
            case breaking([News])
            case top([News])
            case dailyBriefings([Continent: [News]])
            case technicalAnalysis([News])
            case specialReport([News])
        }
        
        struct News {
            
            let title: String
            let subtitle: String
            let headlineImageURL: URL
            let datePublished: Date
            let author: String?
            let url: URL
            
        }
        
        let state: State
        
    }
    
}

extension ArticleListTableViewController.ViewModel.News {
    
    init(_ article: DashboardAPI.Response.Dashboard.News) {
        
        self.title = article.title
        self.subtitle = article.description
        self.headlineImageURL = URL(string: article.headlineImageUrl)!
        self.datePublished = Date(timeIntervalSince1970: article.displayTimestamp)
        self.author = article.authors.first?.name
        self.url = URL(string: article.url)!
    }
    
}
