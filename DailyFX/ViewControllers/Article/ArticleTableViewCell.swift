//
//  ArticleTableViewCell.swift
//  DailyFX
//
//  Created by Tine Ramos on 11/01/2022.
//

import Foundation
import UIKit

final class ArticleTableViewCell: UITableViewCell {
    
    struct ViewModel {
        
        let imageURL: UIImage
        let title: String
        let author: String
        
    }
    
    static let reuseIdentifier = "ArticleTableViewCellIdentifier"
    
    private lazy var headlineImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var detailsStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [titleLabel, authorLabel])
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        return label
    }()
    
    private lazy var authorLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        return label
    }()
    
    private let viewModel: ViewModel
    
    init(viewModel: ViewModel) {
        
        self.viewModel = viewModel
        
        super.init(style: .default, reuseIdentifier: Self.reuseIdentifier)
        
        setupView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        
        contentView.addSubview(headlineImageView)
        contentView.addSubview(detailsStackView)
        
        titleLabel.text = viewModel.title
        authorLabel.text = viewModel.author
    }
    
}

extension ArticleTableViewCell.ViewModel {
    
    
    
}
