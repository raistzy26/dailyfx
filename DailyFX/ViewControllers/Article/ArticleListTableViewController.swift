//
//  ArticleListTableViewController.swift
//  DailyFX
//
//  Created by Tine Ramos on 10/01/2022.
//

import Foundation
import UIKit

final class ArticleListTableViewController: UIViewController {
    
    lazy var tableView: UITableView = {
        return UITableView()
    }()
    
    private let presenter: ArticleListPresentable
    
    init(presenter: ArticleListPresentable) {
        self.presenter = presenter
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .yellow
        
        tableView.register(ArticleTableViewCell.self, forCellReuseIdentifier: ArticleTableViewCell.reuseIdentifier)
        
    }
    
    
    
}
