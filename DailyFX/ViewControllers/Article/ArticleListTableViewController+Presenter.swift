//
//  ArticleListTableViewController+Presenter.swift
//  DailyFX
//
//  Created by Tine Ramos on 10/01/2022.
//

import Foundation
import UIKit

protocol ArticleListPresentable {
    
    func loadArticles()
    
}

extension ArticleListTableViewController {
    
    class Presenter: ArticleListPresentable {
        
        private let networkManager: DailyFXAPI
        
        init(networkManager: DailyFXAPI) {
            self.networkManager = networkManager
            
            loadArticles()
        }
        
        func loadArticles() {
            
            networkManager.getDashboard { result in
                
                switch result {
                    
                case .success(let dashboard):
                    break

                case .failure:
                    break
                    
                }
                
            }
            
        }
        
    }
    
}
