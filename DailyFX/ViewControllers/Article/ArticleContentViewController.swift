//
//  ArticleContentViewController.swift
//  DailyFX
//
//  Created by Tine Ramos on 10/01/2022.
//

import Foundation
import UIKit

final class ArticleContentViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .blue
    }
    
}
