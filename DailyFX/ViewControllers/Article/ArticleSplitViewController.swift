//
//  ArticleSplitViewController.swift
//  DailyFX
//
//  Created by Tine Ramos on 08/01/2022.
//

import UIKit

final class ArticleSplitViewController: UISplitViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white

        preferredDisplayMode = .oneBesideSecondary
        delegate = self
        
        let articlePresenter = ArticleListTableViewController.Presenter(networkManager: DailyFXNetworkManager())
        let primaryViewController = ArticleListTableViewController(presenter: articlePresenter)
        
        let secondaryViewController = ArticleContentViewController()
        
        if #available(iOS 14.0, *) {

            setViewController(primaryViewController, for: .primary)
            setViewController(secondaryViewController, for: .secondary)
        } else {
            viewControllers = [primaryViewController, secondaryViewController]
        }
        
    }
    
}

extension ArticleSplitViewController: UISplitViewControllerDelegate {
    
    @available(iOS 14.0, *)
    func splitViewController(_ svc: UISplitViewController, topColumnForCollapsingToProposedTopColumn proposedTopColumn: UISplitViewController.Column) -> UISplitViewController.Column {
        return .primary
    }

}

