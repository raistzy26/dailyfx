//
//  MainTabBarViewController.swift
//  DailyFX
//
//  Created by Tine Ramos on 09/01/2022.
//

import Foundation
import UIKit

final class MainTabBarViewController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        let articlesVC: ArticleSplitViewController
        
        if #available(iOS 14.0, *) {
            articlesVC = ArticleSplitViewController(style: .doubleColumn)
        } else {
            articlesVC = ArticleSplitViewController()
        }
        articlesVC.title = "Articles"
        
        let marketsVC = MarketsTableViewController()
        marketsVC.title = "Markets"
        
        setViewControllers([articlesVC, marketsVC], animated: true)
        
    }
    
}
